<?php

namespace XLabs\TrafficStarsBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use XLabs\TrafficStarsBundle\Queue\ClickTracker\Producer as ClickTrackerProducer;

class ClickNotify extends Event
{
    private $config;
    private $click_tracker_Q;

    public function __construct($config, ClickTrackerProducer $click_tracker_Q)
    {
        $this->config = $config;
        $this->click_tracker_Q = $click_tracker_Q;
    }

    public function onEpochJoin(Event $event)
    {
        $params = $event->getParams();
        if($event->getParam('x_click_id') && !empty($event->getParam('x_click_id')))
        {
            $this->click_tracker_Q->process(array(
                'click_id' => $event->getParam('x_click_id')
            ));
        }
    }
}