<?php

namespace XLabs\TrafficStarsBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use XLabs\TrafficStarsBundle\Request\Request;
use Symfony\Component\HttpFoundation\Cookie;

class ClickTracker
{
    private $config;
    private $request;
    private $session;
    private $event_dispatcher;

    public function __construct($config, RequestStack $request_stack, SessionInterface $session, EventDispatcherInterface $event_dispatcher)
    {
        $this->config = $config;
        $this->request = $request_stack->getCurrentRequest();
        $this->session = $session;
        $this->event_dispatcher = $event_dispatcher;
    }
    
    public function checkClickIdParam(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $tracked_urls = $this->config['tracked_urls'];
        if(in_array($request->get('_route'), $tracked_urls))
        {
            $querystring = $request->getQueryString();
            $querystring = empty($querystring) ? http_build_query($request->request->all()) : $querystring;

            $request = new Request($querystring);
            $params = $request->getData();

            $click_id = false;

            /* This was supposed to be the way to get the click_id, but looks like the parameter has no name:; then we use method below */
            /*if(array_key_exists('click_id', $params))
            {
                $click_id = $params['click_id'];
            }*/

            // looks like the click_id doesnt come as a named parameter, it just shows smth like ?x1=y1&x2=y2&bfoprueiwhgjibowe
            foreach($params as $key => $val)
            {
                if(strlen($key) > 20 && $val == '')
                {
                    $click_id = $key;
                    break;
                }
            }

            if($click_id)
            {
                // this will create the session variable, that will be converted into a cookie
                $this->session->set($this->config['cookie_name'], $click_id);
            }
        }
    }

    public function setCookie(FilterResponseEvent $event)
    {
        $request = $event->getRequest();
        $response = $event->getResponse();
        $tracked_urls = $this->config['tracked_urls'];

        if(in_array($request->get('_route'), $tracked_urls))
        {
            $click_id = $this->session->get($this->config['cookie_name']);
            if($click_id)
            {
                $this->session->remove($this->config['cookie_name']);
                $click_id_cookie = new Cookie($this->config['cookie_name'], $click_id, time() + 14400);
                $response->headers->setCookie($click_id_cookie);
                $event->setResponse($response);
            }
        }
    }
}