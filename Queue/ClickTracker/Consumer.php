<?php

namespace XLabs\TrafficStarsBundle\Queue\ClickTracker;

use XLabs\RabbitMQBundle\RabbitMQ\Consumer as Parent_Consumer;

class Consumer extends Parent_Consumer
{
    // set your custom consumer command name
    protected static $consumer = 'xlabs:traffic_stars:notify';

    // following function is required as it is
    protected function configure()
    {
        $this
            ->setName(self::$consumer)
        ;
    }

    // following function is required as it is
    public function getQueueName()
    {
        return Producer::getQueueName();
    }

    public function callback($msg)
    {
        // this is all sample code to output something on screen
        $container = $this->getApplication()->getKernel()->getContainer();

        $msg = json_decode($msg->body);
        $click_id = $msg->click_id;

        /*$curl_params = array_merge(array(
            'chat_id' => self::$notifications_channel_id,
            //'chat_id' => '@bikinifanaticshq',
            'disable_notification' => !self::$notifications
        ), $params);*/
        $url = 'http://pstb.gopeerclick.com/postback';
        $curl_params = array(
            'cid' => $click_id
        );
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url.'?'.http_build_query($curl_params));
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);

        $response = curl_exec($ch);
        if(curl_error($ch))
        {
            //mail('xavi.mateos@manicamedia.com', 'TrafficStars API ERROR', json_encode(curl_error($ch)));
            //return curl_error($ch);
        }
        curl_close($ch);
        //mail('xavi.mateos@manicamedia.com', 'TrafficStars API SUCCESS', json_encode($response));
        //return json_decode($response, true);

        /*echo "\n".$this->getQueueName()." - ".date('H:i:s d-m-Y')."\n";
        echo "--------------------------------------------\n";
        foreach($msg as $i => $tracker_item)
        {
            echo $i." : ".$tracker_item."\n";
        }*/
    }
}