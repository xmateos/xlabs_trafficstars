TrafficStars Click Tracker.

## Installation ##
Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/trafficstarsbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\TrafficStarsBundle\XLabsTrafficStarsBundle(),
    ];
}
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml

x_labs_traffic_stars:
    cookie_name: '_ts_click_id'
    tracked_urls: ['route_id_1', 'route_id_2', '.....', 'route_id_N'] # where the click id is created (from trafficstars)
    insert_urls: ['route_id_1', 'route_id_2', '.....', 'route_id_N'] # where the cookie will be created (where to append the click_id)
```

## Consumer
API calls to TrafficStars are made from a queue:
``` yml
php bin/console xlabs:traffic_stars:notify
```